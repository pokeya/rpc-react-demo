# rpc-react-demo

This is a simple demo for the [react-pretty-code](https://github.com/pokeyaro/react-pretty-code) library.

**View Vue Project:** [rpc-vue-demo](https://gitlab.com/pokeya/rpc-vue-demo)

## Getting Started

1. Initialize a `React` project

```bash
➜ pnpm create vite my-react-app --template react-swc-ts
➜ cd my-react-app
➜ pnpm i
```

2. Install and initialize the `react-pretty-code` library

```bash
➜ pnpm i react-pretty-code@latest
➜ npx react-pretty-code init
```

3. You can refer to this [project example](https://gitlab.com/pokeya/rpc-react-demo) for usage

```bash
.
├── index.html
├── node_modules
├── package.json
├── public
├── src
│   ├── App.module.css
│   ├── App.tsx              # Please refer to this
│   └── main.tsx
├── tsconfig.json
└── vite.config.ts
```
