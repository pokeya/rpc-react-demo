import React from "react";
import CodeBlock from "react-pretty-code";
import { Metadata, Content } from "./pages/ContentWithJs";
import styles from "./App.module.css";
import githubIcon from "/github.png";

const App: React.FC = () => {
  return (
    <div className={styles.appContainer}>
      <h1>React Pretty Code Demo</h1>
      <a
        className={styles.githubLink}
        href="https://github.com/pokeyaro/react-pretty-code"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img src={githubIcon} alt="GitHub" className={styles.githubIcon} />
        <span className={styles.githubText}>
          If you like it, please give a 🌟 Star! PRs and issues are welcome!
        </span>
      </a>
      <CodeBlock
        lang="html"
        metadata={Metadata}
        showLineNumbers
        showScrollBars
        showBottomLine
        showVerticalDivideLine
        showExtendedButtons
      >
        {Content}
      </CodeBlock>
    </div>
  );
};

export default App;
